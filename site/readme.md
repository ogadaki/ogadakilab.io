# Blog

<!-- Here are some articles on stuff I am working on. -->

<div v-for="
  page in $site.pages
    .filter(x => x.path.startsWith('/blog/'))
    .sort((page1, page2) => new Date(page2.frontmatter.date) - new Date(page1.frontmatter.date))
">
  <div style="padding-top: 40px;">
    <h2>
      <a :href="page.path" style="font-weight: 700;">
        {{ page.title }}
      </a>
    </h2>
    <div style="font-size: smaller; color: #888; margin: 20px 0px;">
      {{ page.frontmatter.date }}
      <span style="padding: 0px 10px;"></span>
      {{ page.readingTime.text }}
    </div>
    {{ page.frontmatter.description }} 
  </div>
</div>
