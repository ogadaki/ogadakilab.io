module.exports = {
  title: 'ogadaki',
  description: 'ogadaki',
  dest: 'public',
  head: [
    ['link', {
      rel: 'alternate',
      type: 'application/rss+xml',
      href: '/rss.xml',
      title: 'ogadaki RSS Feed'
    }]
  ],
  plugins: [ 'vuepress-plugin-reading-time' ],
  themeConfig: {
    nav: [
        { text: 'about', link: '/about.md' }
    ]
  }
}