# ogadaki

I am [Nicolas Decoster](http://twitter.com/ogadaki), a developer from Toulouse,
south of France. I love lots of stuff related to programming. I like creating,
sharing and teaching (and speaking since not so long ago). Some of my current
interests are Vue.js, WebAssembly, Rust and Scratch. I wonder how the act of
programming will evolve in the near future. I hope it will be more engaging for
new people and will be no more limited to professional programmers.

I am programming for a living at Magellium and I am a co-founder and active
member at [la Compagnie du Code](https://www.lacompagnieducode.org) (creative
computing and computational thinking for kids, and more).

This site is built with [VuePress](https://vuepress.vuejs.org/), its source code
can be found [here](https://gitlab.com/ogadaki/ogadaki.gitlab.io), it is built
and deployed by GitLab CI and hosted on GitLab Pages.

Feel free to reach me and discuss on [Twitter](http://twitter.com/ogadaki) or
[GitLab](https://gitlab.com/ogadaki).