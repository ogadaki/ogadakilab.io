---
date: "May 13, 2019"
description: "Starting blogging on programming."
---

# Hello

> *{{ $page.frontmatter.date }}*

Hi! I am [Nicolas Decoster](/about) and I am in the mood to start a blog to
exchange on subjects more or less related to coding, programming and computer
science. So here we go.

Hope you will enjoy it and feel free to reach me to exchange!