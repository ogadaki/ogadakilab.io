---
date: "May 15, 2019"
author: "Nicolas Decoster"
description: "

  A way to fluently reveal code step by step for demos during presentations. A
  bit like live coding but with less stress.

"
---

# Git tags as the heart beats of demos

> *{{ $page.frontmatter.date }} - {{ $page.frontmatter.author }} - {{ $page.readingTime.text }}*


When I make a presentation, I often need to illustrate my talk with some demos
**and** the corresponding code. And sometime it is even better to reveal the
code step by step. Of course there is live coding for that, but it is a risky
process and I am not confident enough to do it during the most stressful talks.

Here is what I am doing to have a similar experience than live coding, but with
much more peace of mind.


## Git tags to reveal demo code step by step

One common way to do this is to use git tags to identify steps and git checkout
to go from one tag to another. Here is a simple example:

```bash
$ git init

$ touch index.html
$ git add index.html
$ git commit -a -m 'create my demo app'
$ git tag step-0

$ echo 'My demo app' > index.html
$ git commit -a -m 'init my demo app with some content'
$ git tag step-1

$ echo 'with a nice feature' >> index.html
$ git commit -a -m 'add some nice feature'
$ git tag step-2
```

Now I can start the demo with:

```bash
$ git checkout -f step-0
```

And switch to the next step with:

```bash
$ git checkout -f step-1
```

And so on.


## Add a bit of automation

One can call the checkout command to go from one step to another or use her
favorite git tool to select it manually. Either way, I found it a bit
complicated and error prone. Nothing very dramatic, but when you are giving a
talk the less friction there is, the less stress you have, and the more you can
focus on the subject and the way you present it to the audience.

One way to avoid to explicitly name the successive tags, is to use a little
command line tool that move forward one step for you. Something like this:

```bash
$ ./tag step +
```

Then, the demo "stepping" will be as simple as repeating this command.

Of course it is also useful to have a command to move backward, in case you
advance to next step to soon. For example:

```bash
$ ./tag step -
```

This tool gets the current git tag, increases (or decreases) the number of
the `step` tag and actually executes the checkout.

This `tag` tool might look something like that:

```bash
#!/bin/bash

tag_base_name=$1

# Either "+" or "-"
sign=$2

# Get the current tag, like "step-4"
current_tag=$( git describe --tags --match "$tag_base_name-*" )

# Extract "4" from "step-4"
number=$( echo "$current_tag" | cut -d "-" -f 2 )

# Get "step-5" if moving forward, or "step-3" if backward.
new_tag=$tag_base_name-$(( $number $sign 1 ))
# The formula is either "$number + 1" or "$number - 1" depending
# on the second command line argument.

# Actually move to the desired step.
git checkout -f "$new_tag"
```

## Git reset to show what's new

So far so good. But using this simple tool on some new demo content, sometime I
don't remember well all the code that were added at a given step. I might miss
something I want to stress out in my talk. What I would love is to have the new
code automatically highlighted in some way, say, like the default git changes
layout in my code editor...

So, obviously, as we use git to record steps and move from one to another, we
can actually use git for that. In fact it as simple as changing the git's head
to the previous step from the current one. Like that:

```bash
$ git checkout -f step-2
Previous HEAD position was 0b6a1ad init my demo app with some content
HEAD is now at 34c3c71 add some nice feature
$ git reset step-1
Unstaged changes after reset:
M       index.html
```

Now, the working directory contains text files which contents are the ones at
`step-2`, and, as the head is at `step-1`, the editor identifies code that has
been added at `step-2`. I can now easily find those changes I wanted to stress
out for the talk. And of course, the demo is at `step-2` and I can show the
actual results.

![Editor with a simple git diff](./editor_simple_git_diff.png)
![Simple demo app](./demo_app.png)

Naturally, the `git reset` above can be added to the `tag` tool I show earlier.
And navigating through the steps of the demo is now as easy as calling the same
two commands: `tag step +` and `tag step -`.

As I often use this technic for talks that demoes web app, for an even better
speaker experience, I use hot reloading and open my editor and the browser
alongside. This way, each time I move to a new step, my code is updated with
changes highlighted in the editor and the browser refreshes itself with the new
content. With one action I am ready to focus on the stuff I want to talk about
at this particular step of my presentation.


## Commits and tags

Now we have a tool to go from one step to another, one has to first identify
those steps. Each step is a piece of code that is committed and tagged with git.
Either you code, commit then tag for each step, or you only code and commit and
choose later what to tag as steps for your demo. The former is easier to do but
you have to be careful on the way you add things. With the later, you defer the
decision at a later time but you have to manually tag a possibly long list of
commits. I often choose the second as I am more confortable in separating the
two: incrementally code all the features and then choose the steps. At the
beginning I used to tag each commit manually: identify its hash then execute the
`git tag` on it. As it was a "bit" tedious, I now do it automatically.

How am I doing that? I code and commit everything, then create a log file from
git with `git log --oneline`, edit this log file to remove commits I don't want
to stop by, reverse the lines order (with `tac`, for example) to have the oldest
commit first, then call a script to automatically tag each commit remaining in
the file:

```bash
#!/bin/bash

commit_hashes_file=$1
tag_base_name=$2

# An `awk` program which does the following:
#   If stdin is:
#     0b6a1ad init my demo app with some content
#     af18a51 create my demo app
#   and `tag_base_name` is "step", this awk program produces:
#     git tag step-0 0b6a1ad
#     git tag step-1 af18a51
commit_hashes_2_git_tag_commands="
  BEGIN { tag_number = 0 } {
    commit_hash = \$1
    printf \"git tag $tag_base_name-%d %s\n\", tag_number, commit_hash
    tag_number++
  }
"

cat "$commit_hashes_file" \
  | awk "$commit_hashes_2_git_tag_commands" \
  | xargs -n1 -0 bash -c # actually execute the `git tag` commands
```

## Conclusion

Those git based tools help me a lot for my demos'n code kind of presentations.
They help me focus on the messages I want to transmit, and if I am in a mood for
live coding, I can always do it and use these tools as fallback if things go
wrong.

You can find the scripts in [denco](https://gitlab.com/ogadaki/denco) repo in
GitLab, and say hello on Twitter [@ogadaki](http://twitter.com/ogadaki).

Comments are welcome as replies to this [tweet](https://twitter.com/ogadaki/status/1128769951580545024).